part of rikulo_connection;

/** Singleton Connection. */
final Connection connection = new Connection._();

/**
 * Network connection information of this device.
 */
class Connection {
  static const String UNKNOWN = "unknown";
  static const String ETHERNET = "ethernet";
  static const String WIFI = "wifi";
  static const String CELL_2G = "2g";
  static const String CELL_3G = "3g";
  static const String CELL_4G = "4g";
  static const String NONE =  "none";

  js.JsObject _connection;
  
  Connection._() {
    _connection = js.context['navigator']['connection'];
    if (_connection == null)
      throw new StateError('Not ready yet.');
  }

  /** connection type */
  String get type => _connection['type'];
}