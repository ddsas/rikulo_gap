part of rikulo_camera;

class CameraImageData {
  final String imageData;
    
  CameraImageData._fromProxy(js.JsObject p)
      : this.imageData = p as String;
}