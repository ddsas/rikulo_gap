part of rikulo_camera;

/** Singleton Camera. */
final Camera camera = new Camera._();

/**
 * Access to the camera application of this device.
 */
class Camera {
  js.JsObject _camera;

  Camera._() {
    _camera = js.context['navigator']['camera'];
    if (_camera == null)
      throw new StateError('Not ready yet.');
  }
  
  /**
  * Takes a photo using the camera or retrieves a photo from the device's
  * album based on the cameraOptoins paremeter. Returns the image as a
  * base64 encoded String or as the URI of an image file.
  */
  Future<CameraImageData> getPicture([CameraOptions options]) {
    Completer cmpl = new Completer();
    var ok = (p) => cmpl.complete(new CameraImageData._fromProxy(p));
    //var fail = (_) => cmpl.completeError(new UnknownError());
	var fail = (error){ print(error); cmpl.completeError(new UnknownError()); };
	var opts = options == null ? null : new js.JsObject.jsify(options._toMap());
    
    _camera.callMethod('getPicture', [ok, fail, opts]);
    return cmpl.future;
  }
  /**
   * Cleans up the image files stored in temporary storage that were taken by
   * the method [getPicture] when the [CameraOptions.sourceType] is set to
   * [PictureSourceType.CAMERA] and [CameraOptions.destinationType] is set
   * to [DestinationType.FILE_URI].
   */
   Future cleanup() {
    Completer cmpl = new Completer();
    var ok = () => cmpl.complete();
    var fail = (_) => cmpl.completeError(new UnknownError());
    
    _camera.callMethod('cleanup', [ok, fail]);
    return cmpl.future;
  }
}
