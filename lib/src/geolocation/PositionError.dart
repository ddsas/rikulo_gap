part of rikulo_geolocation;

/** Error returned when trying to get [Geolocation] position */
class PositionError {
  static const int PERMISSION_DENIED = 1;
  static const int POSITION_UNAVAILABLE = 2;
  static const int TIMEOUT = 3;

  final int code;
  final String message;

  PositionError._fromProxy(js.JsObject p)
      : this.code = p['code'] as int,
        this.message = p['message'] as String;
}