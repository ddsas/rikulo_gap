part of rikulo_geolocation;

/** onSuccess callback function that returns the Position information */
typedef GeolocationSuccessCB(Position pos);
/** onError callback function if fail getting the Positioninformation */
typedef GeolocationErrorCB(PositionError error);

/** Singleton Geolocation. */
final Geolocation geolocation = new Geolocation._();

/**
 * Capture device motion in x, y, and z direction.
 */
class Geolocation {
  js.JsObject _geolocation;
 
  Geolocation._() {
    _geolocation = js.context['navigator']['geolocation'];
    if (_geolocation == null)
      throw new StateError('Not ready yet.');
  }

  /**
   * Returns the current Position of this device.
   * The Position is returned via the success callback function.
   */
   Future<Position> getCurrentPosition([GeolocationOptions options]) {
    Completer cmpl = new Completer();
    var ok = (p) => cmpl.complete(new Position._fromProxy(p));
    var fail = (p) => cmpl.completeError(new PositionError._fromProxy(p));
	var opts = options == null ? null : new js.JsObject.jsify(options._toMap());
    
    _geolocation.callMethod('getCurrentPosition', [ok, fail, opts]);
    return cmpl.future;
  }
  
  /**
   * Register functions to get the Position information in a regular
   * interval. This method returns an associated watchID which you can use to
   * clear this watch via [clearWatch] method.
   *
   * + [success] - success callback function.
   * + [error] - error callback function.
   * + [options] - optional parameter.
   */   
   watchPosition(GeolocationSuccessCB success,
                    [GeolocationErrorCB error, GeolocationOptions options]) {
    var ok = (p) => success(new Position._fromProxy(p));
    var fail = error == null ?
          null : (p) => error(new PositionError._fromProxy(p));
    var opts = options == null ? null : new js.JsObject.jsify(options._toMap());
    
    var id = _geolocation.callMethod('watchPosition', [ok, fail, opts]);
    return id;
  }
  
  /**
   * Stop watching the Position that was associated with the specified
   * watchID.
   *
   * + [watchID] - the watch ID got from [watchPosition] method.
   */
  void clearWatch(var watchID) {
	_geolocation.callMethod('clearWatch', [watchID]);
  }
}
