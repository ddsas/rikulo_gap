library rikulo_accelerometer;

import 'dart:async';
import 'dart:js' as js;
import 'device.dart';
import 'unknownError.dart';

part 'src/acceleration/Acceleration.dart';
part 'src/acceleration/Accelerometer.dart';
part 'src/acceleration/AccelerometerOptions.dart';